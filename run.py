#!/usr/bin/env python3

import re
import time
from typing import AnyStr
from typing import List

import aiohttp
import asyncio
from user_agent import generate_user_agent


class SitemapCollector:
    def __init__(self, url):
        self.url = url

        self.headers = {"User-Agent": generate_user_agent()}

        self.sitemap_urls, self.urls = set(), set()

        self.is_finished, self.is_sent_to_queue = False, False

    @staticmethod
    def get_sitemap_urls(response: AnyStr) -> List:
        """
        Finds all sitemap urls between <loc></loc> tags.
        """

        return re.findall("<loc>(.*?)</loc>", response)

    async def collect_sitemap_urls(self, session: aiohttp.client.ClientSession) -> None:
        """
        Collects basic sitemap urls.
        """

        async with session.get(url=self.url, headers=self.headers) as r:
            _response = await r.text(encoding="utf-8")

            _urls = self.get_sitemap_urls(response=_response)

            self.sitemap_urls.update(_urls)

    async def collect_urls(self, session: aiohttp.client.ClientSession, url: AnyStr) -> None:
        """
        Goes into every *.xml url and parse its content to retrieve all possible urls.
        """

        async with session.get(url=url, headers=self.headers) as r:
            _response = await r.text(encoding="utf-8")

            _urls = self.get_sitemap_urls(response=_response)

            self.urls.update(_urls)

    async def make_tasks_for_urls(self, session: aiohttp.client.ClientSession) -> None:
        """
        Makes tasks for every *.xml url.
        """

        tasks = [asyncio.gather(self.collect_urls(session, url)) for url in self.sitemap_urls]

        await asyncio.gather(*tasks)

        self.is_finished = True

    async def run(self) -> None:
        """
        Sitemap collector
        """

        async with aiohttp.ClientSession() as session:
            await self.collect_sitemap_urls(session=session)
            await self.make_tasks_for_urls(session=session)


async def main() -> None:
    """
    Main program.
    """

    sitemap_url = 'https://www.otodom.pl/sitemap.xml'

    print(f'\n[OTODOM] --> {sitemap_url}')

    sc = SitemapCollector(url=sitemap_url)

    start = time.time()

    await sc.run()

    end = time.time()

    print(f'[OTODOM] crawled -->  {len(sc.urls)} URLs.')
    print(f'[OTODOM] took time -->  {end - start} s')

    with open('./sitemap_urls.txt', 'a') as f:
        for url in sc.urls:
            f.write(f'{url}\n')


asyncio.run(main())
